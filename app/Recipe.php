<?php

namespace App;

use Illuminate\Support\Facades\Storage;

class Recipe
{
    private const XML_PATH = "/cookbook.xml";
    
    public static function get(string $url)
    {        
        if (!Storage::exists(Recipe::XML_PATH)) return NULL;
        
        $xmlRecipeFileContent = Storage::get(Recipe::XML_PATH);
        $cookbook = simplexml_load_string($xmlRecipeFileContent);
        
        foreach ($cookbook->recipe as $recipe) {
            if ($recipe->url == $url) {
                return $recipe;
            }
        }
        return NULL;
    }
}
