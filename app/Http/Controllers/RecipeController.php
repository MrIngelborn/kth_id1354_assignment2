<?php
    
namespace App\Http\Controllers;

use App\RecipeComment;
use App\Recipe;

class RecipeController extends Controller
{
    /**
    *   Display a recipe
    *   
    *   @param $name Name (id) of the recipe
    */
    public function show(string $name)
    {
        $recipe = Recipe::get($name);
        
        if ($recipe == NULL) {
            abort(404, "Could not find recipe");
        }
        
        $comments = RecipeComment::where('recipe_comments.recipe', $name)->get();
        
        return view('recipe')
                ->with('recipe', $recipe)
                ->with('name', $name)
                ->with('comments', $comments);
    }
}