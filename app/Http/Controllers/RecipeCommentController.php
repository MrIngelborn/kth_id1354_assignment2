<?php
namespace App\Http\Controllers;

use App\RecipeComment;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;

class RecipeCommentController extends Controller
{
    public function create(Request $request, string $name)
    {
        if (!empty($request->comment) && Auth::check()) {
            $comment = new RecipeComment;
            $comment->user_id = Auth::id();
            $comment->recipe = $name;
            $comment->comment = $request->comment;
            $comment->save();
        }
        return redirect(URL::previous());
    }
    
    public function destroy(Request $request, string $name, int $comment_id)
    {
        RecipeComment::where('id', $comment_id)->where('user_id', Auth::id())->delete();
        
        return redirect(URL::previous());
    }
}