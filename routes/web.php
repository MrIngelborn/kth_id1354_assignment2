<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'index')->name('home');

Route::view('/calendar', 'calendar');

Route::get('/recipe/{name}', 'RecipeController@show');

Route::post('/recipe/{name}/comment/', 'RecipeCommentController@create')->name('comment.create');
Route::delete('/recipe/{name}/comment/{comment}', 'RecipeCommentController@destroy')->name('comment.destroy');

Auth::routes();
