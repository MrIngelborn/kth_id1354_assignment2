@extends('layouts.app')

@section('css')
    @parent
	<link type="text/css" rel="stylesheet" href="{{ asset('css/form.css') }}">
	<link type="text/css" rel="stylesheet" media="screen and (min-width:600px)" href="{{ asset('css/form_600.css') }}">
@endsection

@section('title', 'Login')

@section('content_class', 'form')

@section('login') @endsection

@section('content')
    <h1>{{ __('Login') }}</h1>
    
    <form method="POST" action="{{ route('login') }}">
        @csrf
        <label for="email">{{ __('E-Mail Address') }}</label>
        <input
                id="email" 
                type="email" 
                class="{{ $errors->has('email') ? ' invalid' : '' }}" 
                name="email" 
                value="{{ old('email') }}"
                required="required"
                autofocus="autofocus"
        />
        @if ($errors->has('email'))
            <span class="error">{{ $errors->first('email') }}</span>
        @endif
        
        <label for="password">{{ __('Password') }}</label>
        <input
                id="password" 
                type="password" 
                class="{{ $errors->has('password') ? 'invalid' : '' }}" 
                name="password" 
                required="required" 
        />
        @if ($errors->has('password'))
            <span class="error">{{ $errors->first('password') }}</span>
        @endif
        
        <label for="remember">{{ __('Remember Me') }}</label>
        <input 
                type="checkbox"
                name="remember"
                id="remember" {{ old('remember') ? 'checked="checked"' : '' }} 
        />
        
        <input type="submit" value="{{ __('Login') }}" />
        
        <a href="{{ route('register') }}">
            <button type="button">{{ __('Register') }}</button>
        </a>
    
        <a href="{{ route('password.request') }}">
            <button type="button">{{ __('Forgot Your Password?') }}</button>
        </a>
    </form>
@endsection
