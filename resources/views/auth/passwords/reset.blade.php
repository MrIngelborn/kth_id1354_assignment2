@extends('layouts.app')

@section('content')
    <h1>{{ __('Reset Password') }}</h1>
    
    <form method="POST" action="{{ route('password.update') }}">
        @csrf

        <input type="hidden" name="token" value="{{ $token }}">
        
        <label for="email">{{ __('E-Mail Address') }}</label>
        <input 
                id="email"
                type="email"
                class="{{ $errors->has('email') ? 'invalid' : '' }}"
                name="email"
                value="{{ $email ?? old('email') }}"
                required="required"
                autofocus="autofocus"
        />
        @if ($errors->has('email'))
            <span class="error">{{ $errors->first('email') }}</span>
        @endif
        
        <label for="password">{{ __('Password') }}</label>
        <input
                id="password" 
                type="password" 
                class="{{ $errors->has('password') ? 'invalid' : '' }}" 
                name="password" 
                required="required"
        />
        @if ($errors->has('password'))
            <span class="error">{{ $errors->first('password') }}</span>
        @endif
        
        <label for="password-confirm">{{ __('Confirm Password') }}</label>
        <input 
                id="password-confirm" 
                type="password" 
                name="password_confirmation" 
                required="required"
        />
        
        <input type="submit" value="{{ __('Reset Password') }}" />
    </form>
@endsection
